import numpy as np
image1 = np.loadtxt('img1.txt', skiprows = 2)
image2 = np.loadtxt('img2.txt', skiprows = 2)
x = 0
y = 0
def findCoordinates(data):
    x = 0
    y = 0
    check = False
    for rows in data:
        for column in rows:
            if column == 1:
                return x,y
            x += 1
        y += 1

coordinates1 = findCoordinates(image1)
coordinates2 = findCoordinates(image2)
if coordinates1[0] > coordinates2[0]:
    x = coordinates1[0] - coordinates2[0]
    y = coordinates1[1] - coordinates2[1]
if coordinates1[0] < coordinates2[0]:
    x = coordinates2[0] - coordinates1[0]
    y = coordinates2[1] - coordinates1[1]
print('смещение по x: %d\nсмещение по y: %d' % (x, y))

    
                
