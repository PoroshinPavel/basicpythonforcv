import numpy as np

files = ['figure1.txt','figure2.txt','figure3.txt','figure4.txt','figure5.txt','figure6.txt']

            
def findNominalWidth(file):
    currentFile = np.loadtxt(file, skiprows=2)
    realWidth = int(open(file).readlines()[0][0])
    maxLen = 0
    for row in currentFile:
        sumElements = 0
        for element in row:
            if element == 1:
                sumElements+=1
        if sumElements > maxLen:
            maxLen = sumElements
    if maxLen == 0:
        return print('Объект на изображении %s отсутствует' %(file))
    nominalWidth = realWidth / maxLen
    return print('Номинальное разрешение объекта на изображении %s = %f мм' % (file, nominalWidth))

for fileName in files:
    findNominalWidth(fileName)
        
            